# Scripting Toolbox

Provides a few simple tools for easier scripting both internally and externally. The controls are located in the sidebar for the text editor. In addition to the below, it also stores the filepaths to any external scripts in the .blend file, so they're remembered when you re-open the file.

Manual tracking will be implemented at some point.

If you have any ideas for new tools, please open an issue.

#### Console Ouput
System Console - Output to the terminal blender was started from.

Python Console - Output to the python console in the Scripting tab.

Clear Python Console - Clears the python console before running the script.

Keep Banner - Keeps the python banner when the python console is cleared.

#### Track External Scripts
Track - Keep track of external file's modification time and update the text editor. Single option for all scripts, but only applies to the active script.

Autorun - When the script is updated also run the script. Will work even if you're not in the scripting tab.

#### Track Script Objects
Autotrack - Automatically track objects created by the script. Next time the script is run, the objects will be deleted first.

Keep - Keep the objects from the last run of this script. This is per script.


#### Run Script
When running the script manually, you need to use the custom Run Script operator or the features won't work.


The script will only be run if at least one of these criteria are met:


1. One of the first 30 lines starts with an import statement, eg `import math`, `from mathutils import Matrix`, etc
2. If external, the filename ends in `.py`.
3. If the text name ends in `.py`.


#### Delete All Objects

Deletes all objects in the scene.


#### Free Memory

Frees up memory without having to restart.


### Install

Download the latest release from [here](https://gitlab.com/blender_addons_tt/blender-scripting-toolbox/-/releases/permalink/latest). Extract and use the Install button in Blender's addon preferences.

https://docs.blender.org/manual/en/latest/editors/preferences/addons.html

### Questions/Bugs/Issues/Feature requests etc
If you have any questions or feedback etc please open an issue.
