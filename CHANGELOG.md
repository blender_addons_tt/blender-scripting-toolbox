 
 # v0.1.2

 Add delete-all-objects and release-memory operators. \
 Fix output-to-python-console and deletion of tracked objects.