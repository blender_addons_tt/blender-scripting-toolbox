import os
import re
import bpy
import sys
import traceback
import subprocess
from pathlib import Path
from itertools import islice

# Python module to redirect the sys.stdout
from contextlib import redirect_stdout
# So we can create a file-like object
import io
import builtins as __builtin__

interval = 1

warn = "\033[38;5;196m"
reset = "\033[0m"

import time
def mtime(func):
	def wrapper(*arg, **kwargs):
		start = time.time()
		res = func(*arg, **kwargs)
		end = time.time()
		t = end - start
		if t > 1:
			print(f"Function {func.__name__} took: %.4f sec" % (t))
		else:
			print(f"Function {func.__name__} took: %.2f ms" % (t*1000))
		return res
	return wrapper


def update_text(text, path):
	if str(path) == ".":
		return

	with open(path) as file:
		text.from_string(file.read())


def get_mtime(file):
	return file.stat().st_mtime


# Will this always work?
def get_active_editor():
	for a in bpy.data.screens["Scripting"].areas:
		if a.type == "TEXT_EDITOR":
			return a


def print_exception(name, e):
	print(f"{warn}Exception in {name}:{reset} {e}\n")
	print(traceback.format_exc())
	print("Still running")

@mtime
def cls(banner=False):
	for a in bpy.context.screen.areas:
		if a.type == "CONSOLE":
			for region in a.regions:
				if region.type == 'WINDOW':
					with bpy.context.temp_override(area=a, region=region):
						bpy.ops.console.clear()
						if banner:
							bpy.ops.console.banner()

# @mtime
def get_obj(name):
	try:
		return bpy.data.objects[name]
	except:
		print(f"Can't find object {name}")
		return None

@mtime
def clear(at, console=False, banner=True):
	if not at.strack.keep:
		for i in range(len(at.strack.objs)-1, -1, -1):
			obj = get_obj(at.strack.objs[i].name)
			if obj:
				bpy.data.objects.remove(obj, do_unlink=True)

	at.strack.objs.clear()
	if console:
		cls(banner)


def console_print(*args, **kwargs):
	c = bpy.context
	for s in bpy.data.screens:
		if s.name == "Scripting":
			for a in s.areas:
				if a.type == 'CONSOLE':
					s = " ".join([str(arg) for arg in args])
					with bpy.context.temp_override(window=c.window, area=a, region=a.regions[-1]):
						for line in s.split("\n"):
							bpy.ops.console.scrollback_append(text=line, type="OUTPUT")


def track(at, obj=None):
	if obj == None:
		obj = bpy.context.active_object

	item = at.strack.objs.add()
	item.name = obj.name

@mtime
def _run_script(at):
	try:
		at.as_module()
	except Exception as e:
		print_exception(at.name, e)

@mtime
def run_script(at):
	sct = bpy.context.scene.strack

	clear(at, console=sct.clear, banner=sct.banner)

	if at.strack.autotrack:
		objs = [ obj for obj in bpy.data.objects ]

	# our new output
	stdout = io.StringIO()
	with redirect_stdout(stdout):
		_run_script(at)

		if at.strack.autotrack:
			objs2 = [ obj for obj in bpy.data.objects ]
			new_objs = list(set(objs2).difference(objs))
			for o in new_objs:
				track(at, o)

	stdout.seek(0)
	output = stdout.read()
	del stdout

	if sct.p_out:
		console_print(output)
	if sct.c_out:
		__builtin__.print(output) # to system


def get_external_texts():
	texts = bpy.context.scene.strack.texts
	for t in bpy.data.texts:
		if t.name in texts:
			t.filepath = texts[t.name].filepath


# Python detection
##################

# import statements are usually among the first statements we come to
# if we only check for correct syntax
# there are several other languages that could match
# though they're unlikely to be found within blender
# If we check for blender module imports
# we can almost guarantee that we have a python script
# as other languages are unlikely to have the same modules

# stick with the simple method for now (less restrictions
# on the script), see how it goes
pattern = re.compile(f"import \w*|from \w* import \w*")
nlines = 30

def isPython(at):
	if at.name.endswith(".py"):
		return True

	if at.filepath:
		if at.filepath.endswith(".py"):
			return True

		with open(at.filepath) as f:
			head = list(islice(f, nlines))

	else:
		head = []
		count = 0
		for line in at.lines:
			head.append(line.body)
			count += 1
			if count >= nlines:
				break

	for line in head:
		if pattern.match(line):
			return True
	return False


#########################################################

def poll_file():
	if not bpy.context.scene.strack.track:
		return interval

	if not (ed := get_active_editor()):
		return interval

	if not (at := ed.spaces.active.text):
		return interval

	file = Path(at.filepath)

	# Save filepath for when blender is re-opened
	if not at.name in bpy.context.scene.strack.texts and not str(file) in [ "//", "."]:
		entry = bpy.context.scene.strack.texts.add()
		entry.name = at.name
		entry.filepath = at.filepath

	# BUG: If script takes a long time to run
	# then the text doesn't get updated until
	# the script is finished
	if file.exists():
		if (mtime := get_mtime(file)) != float(at.strack.mtime):
			at.strack.mtime = str(mtime)
			update_text(at, file)
			ed.tag_redraw()
			if bpy.context.scene.strack.autorun:
				# Ignore non-python files
				if isPython(at):
					# if not bpy.app.timers.is_registered(poll_file):
					# 	bpy.app.timers.register(run_script, first_interval=0.001)
					run_script(at)
	return interval


def toggle_poll(self, context):
	if bpy.context.scene.strack.track:
		if not bpy.app.timers.is_registered(poll_file):
			bpy.app.timers.register(poll_file, persistent=True)
	else:
		if bpy.app.timers.is_registered(poll_file):
			bpy.app.timers.unregister(poll_file)


class runScriptOperator(bpy.types.Operator):
	bl_idname = "text.strack_run_script"
	bl_label = "Run Script"
	bl_description = "Run script through strack"

	def execute(self, context):
		if not (ed := get_active_editor()):
			return {"CANCELLED"}

		at = ed.spaces.active.text
		# Ignore non-python files
		if isPython(at):
			run_script(at)

		return {'FINISHED'}


class deleteAllOperator(bpy.types.Operator):
	bl_idname = "text.deleteall"
	bl_label = "Delete All Objects"
	bl_description = "Delete all objetcs"


	def execute(self, context):
		deleteAll()

		return {'FINISHED'}

@mtime
def deleteAll():
	for obj in bpy.data.objects:
		bpy.data.objects.remove(obj, do_unlink=True)


class releaseMemOperator(bpy.types.Operator):
	bl_idname = "text.release_mem"
	bl_label = "Release memory"
	bl_description = "Release memory without retarting"


	def execute(self, context):

		releaseMem()
		return {'FINISHED'}


@mtime
def releaseMem():
	for block in bpy.data.meshes:
		if not block.users:
			bpy.data.meshes.remove(block)

	for block in bpy.data.materials:
		if not block.users:
			bpy.data.materials.remove(block)

	for block in bpy.data.textures:
		if not block.users:
			bpy.data.textures.remove(block)

	for block in bpy.data.images:
		if not block.users:
			bpy.data.images.remove(block)

class strackColPG(bpy.types.PropertyGroup):
	name: bpy.props.StringProperty(name="name")


class strackCol2PG(bpy.types.PropertyGroup):
	name: bpy.props.StringProperty(name="name")
	filepath: bpy.props.StringProperty(name="filepath")

class strackTextPG(bpy.types.PropertyGroup):
	keep: bpy.props.BoolProperty(name="keep", default=False)
	objs: bpy.props.CollectionProperty(name="objs", type=strackColPG)
	mtime: bpy.props.StringProperty(name="mtime", default="-1.0")
	autotrack: bpy.props.BoolProperty(name="autotrack", default=True)


class strackScenePG(bpy.types.PropertyGroup):
	track: bpy.props.BoolProperty(name="track", default=True, update=toggle_poll)
	autorun: bpy.props.BoolProperty(name="autorun", default=True)
	texts: bpy.props.CollectionProperty(name="texts", type=strackCol2PG)

	c_out: bpy.props.BoolProperty(name="c_out", default=True)
	p_out: bpy.props.BoolProperty(name="p_out", default=True)
	clear: bpy.props.BoolProperty(name="clear", default=True)
	banner: bpy.props.BoolProperty(name="banner", default=True)


classes = [
	strackColPG,
	strackCol2PG,
	strackTextPG,
	strackScenePG,
	runScriptOperator,
	releaseMemOperator,
	deleteAllOperator,
]

def register():
	for c in classes:
		bpy.utils.register_class(c)

	bpy.types.Text.strack = bpy.props.PointerProperty(type=strackTextPG)
	bpy.types.Scene.strack = bpy.props.PointerProperty(type=strackScenePG)

	bpy.app.timers.register(get_external_texts, first_interval=0.00001)
	bpy.app.timers.register(poll_file, persistent=True)


def unregister():
	if bpy.app.timers.is_registered(poll_file):
		bpy.app.timers.unregister(poll_file)

	if bpy.types.Scene.strack:
		del bpy.types.Scene.strack
	if bpy.types.Text.strack:
		del bpy.types.Text.strack

	for c in classes:
		bpy.utils.unregister_class(c)


