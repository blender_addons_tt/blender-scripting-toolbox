import bpy


def get_active_text():
	for a in bpy.data.screens["Scripting"].areas:
		if a.type == "TEXT_EDITOR":
			at = a.spaces.active.text
			return at


def track(obj=None):
	at = get_active_text()

	if at.strack.autotrack:
		return

	if obj == None:
		obj = bpy.context.active_object

	if not isTracked(obj):
		item = at.strack.objs.add()
		item.name = obj.name


def printy():
	at = get_active_text()
	if len(at.strack.objs) == 0:
		print("\nTracked Objects: None")
	else:
		print("\nTracked Objects:")
		for item in at.strack.objs:
			print("    ", item.name, item.id)


def isTracked(obj=None):
	if obj == None:
		obj = bpy.context.active_object

	at = get_active_text()
	if obj.name in at.strack.objs:
		return True
	return False


